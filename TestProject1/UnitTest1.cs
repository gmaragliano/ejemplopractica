using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;

namespace TestProject1
{   
    [TestFixture]
    public class Tests
    {
        IWebDriver driver;
        
        [SetUp]
        public void Setup()
        {
            // AppDomain -> clase que retorna el directorio donde se ubica el proyecto actual
            driver = new ChromeDriver(@"C:\Users\gmaragliano\Documents\TestProject1\TestProject1\Driver\");
            driver.Manage().Window.Maximize();
        }

        [Test, Order(1)]
        public void TestValidarUrl()
        {
            abrirPagina();
            string url = driver.Url;
            
            Assert.AreEqual("https://www.opencart.com/", url);
        }

        [Test, Order(2)]
        public void TestRedirigirUrl()
        {
            redireccionar();

            IWebElement titulo = driver.FindElement(By.TagName("h1"));

            Assert.AreEqual("Welcome to OpenCart Extension Store", titulo.Text);
                      
        }


        [Test, Order(3)]
        public void TestBuscador()
        {
                        
            //System.Threading.Thread.Sleep(3000);

            buscador("Google");

        }

        [Test, Order(4)]
        public void TestMenuDesplegable()
        {
                        
            menuDesplegable("3.0.3.7");

        }

        [Test, Order(5)]
        public void TestRadioButton()
        {
            redireccionar();
            radioButtons("//section[@id='extension-developed']/div[@class='radio'][3]/label/input");

        }

        [Test, Order(6)]
        public void TestSeleccionProducto()
        {
            string nombreProducto = "Facebook for OpenCart";
            ProductoASeleccionar(nombreProducto);
            IWebElement titulo = driver.FindElement(By.TagName("h3"));
            Assert.AreEqual(titulo.Text, nombreProducto);
            //para buscar un elemento que no esta en esta pagina tenemos que inicializar un for o while que revise todos los elementos de la
            //pagina e inicialice con una variable (bandera) en false. En caso de no encontrar el elemento sigue en false y pasar de pagina
            //seleccionando las mismas a traves del nombre. Cuando encuentre el elemento poner la bandera = true y el break para que termine
            //el ciclo for o while segun corresponda.
        }
        /*[TearDown]

        public void CerrarNavegador()
        {
            driver.Quit();
        }*/

        public void abrirPagina()
        {
            driver.Navigate().GoToUrl("https://www.opencart.com/");
        }

        public void redireccionar()
        {
            abrirPagina();
            IWebElement Element = driver.FindElement(By.XPath("//div[@id='marketplace']/div/div[1]/div[1]/a"));
            Element.Click();
        }

        public void buscador(string palabra)
        {
            redireccionar();
            IWebElement buscador = driver.FindElement(By.XPath("//body/div[@id='marketplace-extension']/div[2]/div[1]/div[1]/section[1]/div[1]/input[1]"));
            buscador.SendKeys(palabra);
            buscador.SendKeys(Keys.Enter);

            /*IWebElement botonBuscar = driver.FindElement(By.Id("button-search"));
            botonBuscar.Click();*/
        }

        public void menuDesplegable (string palabra)
        {
            redireccionar();
            SelectElement menu = new SelectElement(driver.FindElement(By.XPath("//section[@id='extension-opencart-version']/div[1]/div/select")));
            menu.SelectByText(palabra);            
        }

        public void radioButtons(string codigoXpath)
        {
            IWebElement radioButton = driver.FindElement(By.XPath(codigoXpath));
            radioButton.Click();
        }

        public void ProductoASeleccionar(string textoASeleccionar)
        {
            redireccionar();
            IWebElement nombreProducto = driver.FindElement(By.LinkText(textoASeleccionar));
            nombreProducto.Click();
                        
        }
    }

}