﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestProject1
{   
    [TestFixture]
    class PruebaUdemy1
    {   
        [Test]
        public void ContieneA()
        {
            string texto = "hola";
            var contieneLetra = texto.Contains("a");
            Assert.IsTrue(contieneLetra,"No contiene la letra");

        }

        [Test]
        public void LongitudTexto()
        {
            var texto = "hola";
            var longitud = texto.Length;
            Assert.AreEqual(4, longitud,"La longitud no coincide: " + texto.Length);

        }
    }
}
